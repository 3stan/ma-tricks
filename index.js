rowCount = 1;
columnCount = 1;

// Array of rows
matrix = [[0]];
outputMatrix = [];

steps = "";

// Wipes the existing displayed matrix and rebuilds the divs
function resizeMatrix() {
    removeMatrixChildren()
    rowCountElement = document.getElementById("row-input");
    columnCountElement = document.getElementById("col-input");
    rowCount = validateMatrixDimension(rowCountElement, "row count");
    columnCount = validateMatrixDimension(columnCountElement, "column count");

    resizeCachedMatrix();


    for (let col = 1; col <= columnCount; col++){
        addColumn(col);
    }
}

function validateMatrixDimension(element, countType){
    var count = element.value;
    var validCount = parseInt(count);
    if (validCount > 10) {
        validCount = 10;
        document.getElementById("errors").innerHTML = "<p class='errors'>Constrained " + countType + " to 10.</p>";
    }
    if (validCount < 0) {
        validCount = 0;
        document.getElementById("errors").innerHTML = "<p class='errors'>Constrained " + countType + " to 0.</p>";
    }
    element.value = validCount
    return validCount;
}

function displayOutput() {
    removeMatrixChildren("output");
    for (let col = 1; col <= columnCount; col++){
        addOutputColumn(col);
    }
}

function addOutputColumn(colNumber) {
    var columnId = "output-column-" + colNumber;
    var newColumn = createCellColumnElement(colNumber, columnId);
    document.getElementById("output-matrix").append(newColumn);
    for (j=1; j <= rowCount; j++){
        var cellId = "output-cell-" + j + "," + colNumber;
        var newCell = createCellElement(j, colNumber, false, cellId);
        document.getElementById(columnId).appendChild(newCell);
        document.getElementById(cellId).innerHTML = "<input type='number' value='" + outputMatrix[j-1][colNumber-1] + "' readonly>";
    }
}

function resizeCachedMatrix() {
    var numRows = matrix.length;
    var rowDiff = rowCount - numRows;
    if (rowDiff > 0){
        for (let i = 0; i < rowDiff; i++){
            matrix.push([]);
        }
    } else if (rowDiff < 0){
        for (let i = numRows; i > rowCount; i--){
            matrix.pop();
        }
    }

    for (let i = 0; i < rowCount; i++){
        var numCols = matrix[i].length;
        var colDiff = columnCount - numCols;
        if (colDiff > 0){
            for (let j = numCols; j < columnCount; j++){
                matrix[i].push(0);
            }
        } else if (colDiff < 0){
            for(let j = numCols; j > columnCount; j--){
                matrix[i].pop();
            }
        }
    }
}

// Adds column div and cells 
function addColumn(colNumber) {
    var newColumn = createCellColumnElement(colNumber);
    document.getElementById("input-matrix").append(newColumn);
    for (j=1; j <= rowCount; j++) {
        var newCell = createCellElement(j, colNumber);
        document.getElementById(colNumber).appendChild(newCell);
        document.getElementById("input-" + j + "," + colNumber).value = matrix[j-1][colNumber-1];
    }
}

// Removes child divs of "input-matrix"
function removeMatrixChildren(mtxType = "input") {
    var inputMatrix = document.getElementById(mtxType + "-matrix");
    var errorDiv = document.getElementById("errors");
    errorDiv.innerHTML = "";
    inputMatrix.innerHTML = "";
}

function createCellColumnElement(colNumber, id = colNumber) {
    var newColumn = document.createElement('div');
    newColumn.className = 'cell-column';
    newColumn.id = id;
    return newColumn;
}

function createCellElement(rowNumber, colNumber, includeInput = true, id = rowNumber + ',' + colNumber) {
    var newCell = document.createElement('div');
    if (includeInput){
        var newInput = createCellInputElement(id);
        newCell.appendChild(newInput);
    }
    newCell.className = 'cell';
    newCell.id = id;
    return newCell;
}

function setCoordinateValue(id) {
    var value = validateCoordinateValue(document.getElementById("input-" + id), id);
    var splitId = id.split(",");
    var row = splitId[0];
    var col = splitId[1];

    matrix[row - 1][col - 1] = value;
}

function validateCoordinateValue(element, coordinate) {
    var value = element.value;
    var validValue = parseInt(value);
    if (validValue > 50) {
        validValue = 50;
        document.getElementById("errors").innerHTML = "<p class='errors'>Constrained (" + coordinate + ") to 50.</p>";
    }
    if (validValue < -50){
        validValue = -50;
        document.getElementById("errors").innerHTML = "<p class='errors'>Constrained (" + coordinate + ") to -50.</p>";
    }
    element.value = validValue;
    return validValue;
}

function createCellInputElement(id) {
    var newInput = document.createElement("input");
    newInput.id = "input-" + id;
    newInput.setAttribute("type", "number");
    newInput.addEventListener("change", (event) => {
        setCoordinateValue(event.target.parentNode.id);
    });
    return newInput;
}

function reduce() {
    document.getElementById("steps").innerHTML = "";
    steps = "</br>Sorting the rows of the input matrix by their first entry.</br>";
    var m = sortRows(matrix.slice(0));
    var pivots = [];

    // Echelon Form
    for(let c = 0; c < matrix[0].length; c++){
        var col = getColumn(m, c);
        var pivotIndex = undefined;
        for (let r = pivots.length; r < col.length; r++) {
            // found pivot
            if (pivotIndex == undefined && m[r][c] != 0){
                // scale row so pivot is the leftmost non-zero value of 1
                if (m[r][c] != 1){
                    let row = m[r];
                    let value = m[r][c];
                    let newRow = scaleRow(row, 1 / value);
                    steps += "</br>Scaling row " + r + " by " + 1 / value+ ".</br>";
                    m[r] = newRow;
                    steps += matrixToString(m);
                }
                pivotIndex = getPivotPosition(pivots, c);
                if (r != pivotIndex){
                    m = swapRow(m, r, pivotIndex);
                    steps += matrixToString(m);
                }
                pivots.push({col: c, row: pivotIndex});
                continue;
            }
            // have a pivot and need to eliminate non-zero
            if (pivotIndex != undefined && m[r][c] != 0){
                m[r] = addRow(m, r, scaleRow(m[pivotIndex], -m[r][c]), pivotIndex, -m[r][c])
                steps += matrixToString(m);
            }
        }
    }

    // Reduced Echelon Form
    for (let c = 0; c < matrix[0].length; c++){
        var col = getColumn(m, c);
        var pivot = getPivotForColumn(pivots, c);
        var elementsToReduce = [];
        for (let r = 0; r < col.length; r++){
            if (pivot === undefined){
                continue;
            }
            if (pivot !== undefined && pivot.row == r){
                continue;
            }
            if (pivot !== undefined && m[r][c] != 0){
                let row = m[r];
                let value = m[r][c];
                let addend = scaleRow(m[pivot.row], -m[r][c]);
                m[r] = addRow(m, r, addend, pivot.row, -m[r][c]);
                steps += matrixToString(m);
            }
        }
    }

    outputMatrix = m;
    displayOutput();
    document.getElementById("steps").innerHTML = steps;
}

function getPivotForColumn(pivots, col){
    for (let i = 0; i < pivots.length; i++){
        console.log(pivots[i] + " " + col);
        if (pivots[i].col == col){
            return pivots[i];
        }
    }
    return undefined;
}

function getPivotPosition(pivots, col) {
    var x = 0;
    for (let i = 0; i < col; i ++){
        if (typeof pivots[i] !== undefined){
            x++;
        }
    }
    return x;
}

function rowToString(row){
    var message = "[";
    for (let i = 0; i < row.length; i++){
        message += row[i]
        if (row[i+1] !== undefined){
            message += ",";
        }
    }
    return message += "]";
}

function matrixToString(matrix) {
    var message = "";
    for (let i = 0; i < matrix.length; i++){
        message += rowToString(matrix[i]) + "</br>"
    }
    message += "</br>";
    return message;
}

function addRow(matrix, index, addend, addendIndex, scalar) {
    steps += "</br>Adding " + scalar + " times row " + addendIndex + " (" + rowToString(addend) + ") to row " + index +" (" + rowToString(matrix[index])+ ".</br>"
    var result = []
    var row1 = matrix[index];
    for (let i = 0; i < row1.length; i++) {
        result.push(row1[i] + addend[i]);
    }
    return result;
}

function scaleRow(row, scalar) {
    var result = [];
    for (let i = 0; i < row.length; i ++){
        result.push(row[i] * scalar)
    }
    return result;
}

function swapRow(matrix, index1, index2) {
    steps += "</br>Swapping row " + index1 + " with row " + index2 + ".</br>";
    tmp1 = matrix[index1];
    matrix[index1] = matrix[index2];
    matrix[index2] = tmp1;
    return matrix
}

function getColumn(matrix, index) {
    var col = [];
    for (let i = 0; i < matrix.length; i++){
        col.push(matrix[i][index])
    }
    return col;
}

function isColumnEchelon(column) {
    var pivotIndex = undefined;
    for (let i = 0; i < column.length; i++){
        if (pivotIndex != undefined && column[i] != 0){
            return false;
        }
        if (pivotIndex == undefined && column[i] == 1){
            pivotIndex = i;
            continue;
        }
        if (pivotIndex == undefined && column[i] != 0) {
            return false;
        }
    }
    return true;
}

function sortRows(matrix) {
    return matrix.sort(function(a, b){return a[0] - b[0]});
}